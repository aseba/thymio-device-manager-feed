--[[
LuCI Aseba - Lua Configuration Interface for Thymio Device Manager
Copyright (C) 2017, 2023 Inria
Author David James Sherman <david.sherman@inria.fr>
]]--

require("luci.model.uci")

m = Map("thymio-device-manager", "Thymio device management services",
	"Bridge Thymio robots and remote network clients")

--- Thymio device manager, /etc/config/thymio-device-manager

s = m:section(NamedSection, "tdm", "thymio-device-manager",
	      "Thymio Device Manager",
	      "thymio-device-manager dæmon")
s.addremove = false
s.anonymous = true

active = s:option(Flag, "active", "Active?")
active.enabled = "true"
active.disabled = "false"

tcp_port = s:option(Value, "tcp_port", "TCP Port")
tcp_port.datatype = "port"
tcp_port.default = 8596

ws_port = s:option(Value, "ws_port", "Websocket Port")
ws_port.datatype = "port"
ws_port.default = 8597

log_level = s:option(ListValue, "log_level", "Log Level")
log_level:value("critical")
log_level:value("err")
log_level:value("warn")
log_level:value("info")
log_level:value("debug")
log_level:value("trace")
log_level.default = "warn"

allow_remote = s:option(Flag, "allow_remote", "Allow remote connections?")
allow_remote.enabled = "true"
allow_remote.disabled = "false"

password = s:option(Value, "password", "Remote connection password")
password.default = "THYMIO"

comment = s:option(DummyValue, "uuid", "UUID", "Service UUID used by mDNS discovery")

---

return m
