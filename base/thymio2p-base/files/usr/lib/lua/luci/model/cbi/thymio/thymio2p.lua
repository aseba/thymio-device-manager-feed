--[[
LuCI Aseba - Lua Configuration Interface for Thymio Device Manager
Copyright (C) 2017, 2023 Inria
Author David James Sherman <david.sherman@inria.fr>
]]--

require("luci.model.uci")

m = Map("thymio2p", "Thymio 2+ Wireless Modes",
	"Configure access point and client wireless modes for the Thymio 2+ mini-router")

--- Thymio 2+, /etc/config/thymio2p

--- Section "Mode selection"

choose = m:section(NamedSection, "choose", "thymio2p",
	      "Mode selection",
	      "How to select the wireless mode")
choose.addremove = false
choose.anonymous = true

--- Hint
comment = choose:option(DummyValue, "note", "", "Hint: as a failsafe, activate the slide switch before choosing Client mode")

--- Option choose.switch
active = choose:option(Flag, "switch", "Activate switch",
	       "Use the slide switch to choose the wireless mode")
active.enabled = 'true'
active.disabled = 'false'

--- Option choose.status
mode = choose:option(ListValue, "status", "Current mode",
	       "Choose the wireless mode")
mode:value("access-point")
mode:value("client")
mode.default = "access-point"

--- Hook to change wireless mode
function m.on_after_commit(self)
   local new_mode = self:get("choose", "status")
   luci.sys.call("logger -t MOBSYA thymio2p luci after_commit, reloading thymio2p service to "..new_mode)
   luci.sys.call("/usr/bin/choose-thymio2p-mode "..new_mode)
end

--- Section "Access point mode parameters"

s = m:section(NamedSection, "default_radio0", "wifi-iface",
	      "Access point mode parameters",
	      "Specify wireless parameters to use when in Access point mode")
s.addremove = false
s.anonymous = true

ssid = s:option(Value, "ssid", "Thymio 2+ network",
		"Wireless network to create")
ssid.default = 'Thymio'

key = s:option(Value, "key", "Wireless password",
	       "Choose a password for the new network")
key.default = 'MYTHYMIO'

encryption = s:option(ListValue, "encryption", "Encryption")
encryption:value("psk2")
encryption:value("psk2+tkip")
encryption.default = 'psk2'

--- Section "Client mode parameters"

s = m:section(NamedSection, "wifinet1", "wifi-iface",
	      "Client mode parameters",
	      "Specify wireless parameters to use when in Client mode")
s.addremove = false
s.anonymous = true

ssid = s:option(Value, "ssid", "Wifi network",
		"Existing wireless network to connect to as client")
ssid.default = 'Thymio'

key = s:option(Value, "key", "Wireless password",
	       "Provide the password for the existing network")
key.default = 'MYTHYMIO'

encryption = s:option(ListValue, "encryption", "Encryption")
encryption:value("sae-mixed")
encryption:value("psk2")
encryption:value("psk2+tkip")
encryption:value("wpa3-mixed")
encryption:value("psk-mixed")
encryption:value("wep")
encryption.default = 'psk2'

---

return m
