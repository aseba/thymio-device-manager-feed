--[[
LuCI Aseba - Lua Configuration Interface for Thymio Device Manager
Copyright (C) 2017, 2023 Inria
Author David James Sherman <david.sherman@inria.fr>
]]--

module("luci.controller.thymio.thymio", package.seeall)

function index()
	entry({"admin", "thymio"}, firstchild(), "Thymio", 50).dependent=false
	entry({"admin", "thymio", "thymio-device-manager"}, cbi("thymio/thymio-device-manager"), "Thymio Device Manager", 1).dependent=false
	entry({"admin", "thymio", "thymio2p"}, cbi("thymio/thymio2p"), "Thymio 2+ Wireless Modes", 2).dependent=false
end
