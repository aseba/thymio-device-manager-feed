#!/bin/bash

: ${DEST:=${1:-$PWD}}

: ${OWNER:="Mobsya"}
: ${REPO:="thymio-platform"}
: ${TAG:="v2.1.4"}
: ${ARTIFACT:="openwrt-config-dashboard.zip"}
: ${TOKEN:="$(cat token 2> /dev/null)"}

BASE_URI="https://api.github.com/repos/${OWNER}/${REPO}"
RELEASES_URI="${BASE_URI}/releases"
ACCEPT_JSON="Accept: application/vnd.github+json"
ACCEPT_BIN="Accept: application/octet-stream"

gh_curl() { curl --location --silent \
	    -H "Authorization: Bearer ${TOKEN}" \
	    -H "X-GitHub-Api-Version: 2022-11-28" \
	    "$@" ; }

release_asset() { $HOME/.local/bin/jq ".[] | select(.tag_name==\"${TAG}\") | .assets[] | select(.name==\"${ARTIFACT}\") | .id" | tr -d \" ; }

ASSET_ID=$(gh_curl -H "${ACCEPT_JSON}" "${RELEASES_URI}" | tee /tmp/releases.json | release_asset)
echo ASSET_ID=${ASSET_ID:?Asset id not found!} 1>&2

mkdir -p "${DEST}"
gh_curl -H "${ACCEPT_BIN}" -o /tmp/${ARTIFACT} "${RELEASES_URI}/assets/${ASSET_ID}"
unzip -o -d "${DEST}" /tmp/${ARTIFACT}
