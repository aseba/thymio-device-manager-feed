This is an [OpenWRT](https://openwrt.org) package feed for adding the
Thymio Device Manager (TDM) from the Thymio Suite as a standalone network
service.

To compile this package using the OpenWRT SDK, add the following line to
`feeds.conf`:

```
src-git thymio git://gitlab.inria.fr/aseba/thymio-device-manager-feed.git
```

Then, update the feed and add the package to menuconfig:
```shell
./scripts/feeds update thymio
./scripts/feeds install -a -p thymio-device-manager
```

The Thymio Device Manager package will appear in the **Networking** section.

Configure and compile with
```shell
make menuconfig
make {tools,toolchain}/install
make
```
